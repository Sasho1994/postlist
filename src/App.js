import React from 'react';
import logo from './logo.svg';
import './App.css';
import PostListItem from './postList/PostListItem';
import Button from "./button/button";
import Form from "./form/Form";
import styled from "styled-components";

const Div = styled.div `
    max-width: 600px;
    margin: 0 auto;
    ul {
        list-style: none;
        padding: 0;
        
    }
`;


const PostList = () => fetch('https://jsonplaceholder.typicode.com/posts/')
    .then(response =>
        response.json());

class App extends React.Component {
    constructor() {
        super();
        this.state = {
            post: [],
            filterPost: [],
            itemToShow: 10,
            isLoading: true,
            step: 10
        };
        this.handleClick = this.handleClick.bind(this);
        this.changeStateForm = this.changeStateForm.bind(this);
    }

    async componentDidMount() {
        setTimeout(() => {
            PostList().then(data => {
                this.setState(
                    {
                        post: data.slice(0, this.state.itemToShow),
                        isLoading: false,
                        filterPost: data.slice(0, this.state.itemToShow),
                        allPost: data
                    }
                );
            })
        },2000)
    }

    handleClick() {
        let num = this.state.itemToShow + this.state.step;
        this.setState({
            itemToShow: num,
        });
        const {allPost} = this.state;
        this.setState({
            post: allPost.slice(0, num),
            filterPost: allPost.slice(0, num)
        });
    }

    changeStateForm(el) {
        let val = el.target.value,
            dataPost = this.state.post;

        let newFilterData = dataPost.filter(item => {
            return item.title.includes(val)
        });
        this.setState({
            filterPost: newFilterData
        })
    }

    render() {

        return (
            <Div>
                {!this.state.isLoading ?
                    <>
                        <Form handleChangeText={this.changeStateForm}/>
                        <ul className="PostList">
                            {this.state.filterPost.map(item =>
                                <PostListItem key={item.id}  title={item.title} body={item.body}/>
                            )}
                        </ul>
                        {this.state.itemToShow < 100? <Button handleClick={this.handleClick}/> : null }

                    </> : <div style={{
                        position: "absolute",
                        left: "50%",
                        top: "50%",
                        transform: "translate(-50%,-50%)"
                    }}>
                        <img src={logo} className="App-logo" alt="logo"/>
                    </div>
                }
            </Div>
        );
    }
}

export default App;
