import React from "react";
import styled from "styled-components";
import T from "prop-types";

const StyledButton = styled.button`
    background: transparent;
    font-size: 18px;
    color: #29a6ff;
    border: 1px solid #29a6ff;
    padding: 8px 15px;
    box-shadow: none;
    transition: .3s;
    cursor: pointer;
    vertical-align: middle;
    border-radius: 5px;
    &:hover {
        background: #29a6ff;
        color: #fff;
    }
`;

const Button = ({handleClick}) =>
    <StyledButton onClick={handleClick}>
        Show More
    </StyledButton>;

Button.T = {
    handleClick: T.func,
};

export default Button;