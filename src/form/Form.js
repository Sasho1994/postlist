import React from "react";
import styled from "styled-components";
import T from "styled-components";

const Input = styled.input`
    height: 35px;
    width: 100%;
    margin-right: 20px;
    border-radius: 5px;
    box-shadow: none;
    border: 1px solid #7ac8ff;
    vertical-align: middle;    
`;

const Form = ({handleChangeText}) => {
    return (
        <form onChange={handleChangeText}>
            <Input type="text"/>
        </form>
    )
};

Input.T = {
  handleChangeText: T.func
};

export default Form;
