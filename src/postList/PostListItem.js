import React from "react";
import T from "prop-types";

const PostListItem = ({title, body}) => {
    return (
    <li >
        <h3>
             {title}
        </h3>
        <div>
             {body}
        </div>
    </li>
    )
};

PostListItem.T = {
  title: T.string,
  body: T.string
};

export default PostListItem;